/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
 */

#define CONFIG_QCOM_SOCINFO_DT 1
#define CONFIG_MSM_QUIN_SUBSYSTEM_NOTIF_VIRT 1
