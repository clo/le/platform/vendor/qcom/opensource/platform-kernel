/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
 */

#define CONFIG_QCOM_ADSP_VOTE_SMP2P 1
#define CONFIG_PM_SILENT_MODE 1
